import React from 'react'

export class SignInButton extends React.Component {
  // I tried with only href = "/signIn" and it should go to the server side since I used proxy = "http://localhost:8080" in package.json, but it does not work, why?
  render(){
    return(
      <div>
        <h1 class="display-4 text-left">
          <a href= "http://localhost:8080/signIn" role="button">Connect with spotify</a>
        </h1>
      </div>
    )
  }
}
