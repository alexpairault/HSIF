import React from 'react';

export function Footer () {
  return(
    <div class = "col-sm-5">
      <p class = "text-left">This application has been developed by Alex Pairault in December 2019 using
        <ul>
          <li>Node.js/Express.js</li>
          <li>React.js</li>
          <li>MongoDB Atlas</li>
          <li>Hosted on AWS</li>
        </ul>
      </p>
    </div>
  )
}
