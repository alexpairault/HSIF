import React from 'react'

export function BigThanksTo () {
  return(
    <div class = "col-sm-5">
      <p class = "text-left">Big thanks to
        <ul>
          <li>Spotify for providing a well documented API</li>
          <li>Songkick for providing information about the upcoming concerts</li>
        </ul>
      </p>
    </div>
  )
}
