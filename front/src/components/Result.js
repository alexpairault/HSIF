import React from 'react';
import {ProfileButton} from './ReusableComponents/ProfileButton.js'
import {LogOutButton} from './ReusableComponents/LogOutButton.js'
import {HomeButton} from './ReusableComponents/HomeButton.js'
import {Redirect} from 'react-router-dom'
import axios from 'axios'
import * as checked from '../images/checked.json'
import Lottie from "react-lottie";

const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: checked.default,
  rendererSettings: {
  preserveAspectRatio: "xMidYMid slice"
  }
}

export class Result extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isLoggedIn: undefined,
      hasJustCreatedPlaylist: undefined,
      _loadingLogInDone: false
    }
  }

    async componentDidMount(){

      let promiseIsLoggedIn = await axios.get('/isLoggedIn')
      this.setState({ isLoggedIn: promiseIsLoggedIn.data.isLoggedIn })
      this.setState({ _loadingLogInDone: true })

      let promiseHasJustCreatedPlaylist = await axios.get('/getResultCreationOfPlaylist')

    }

    render(){
      if (!this.state._loadingLogInDone) {
        return (
          <p>Waiting to get some data</p>
        )
      }
      else if (!this.state.isLoggedIn){
        alert('You are not allowed to be here')
        return <Redirect to="/" />;
      } else {
        return(
          <div>
            <h1 className="display-4 text-left">Congratulations! You have just created a new spotify playlist with artists that will perform very soon in your city</h1>
            <Lottie options={defaultOptions} height={220} width={220} />                      
            <p>Go on your spotify profile to find it</p>
            <HomeButton />
            <ProfileButton />
            <LogOutButton />
          </div>
        )
      }
    }
  }
