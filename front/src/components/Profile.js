import axios from "axios";
import React from 'react';
import {LogOutButton} from './ReusableComponents/LogOutButton.js'
import {HomeButton} from './ReusableComponents/HomeButton.js'
import {Redirect} from 'react-router-dom'
import {PresentationPhraseProfile} from './ProfileComponents/PresentationPhraseProfile.js'
import {Playlists} from './ProfileComponents/Playlists.js'

export class Profile extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isLoggedIn: false,
      _loadingLogInDone: false
    }
  }

  async componentDidMount(){

    let promiseIsLoggedIn = await axios.get('/isLoggedIn')
    this.setState({ isLoggedIn: promiseIsLoggedIn.data.isLoggedIn })
    this.setState({ _loadingLogInDone: true })

  }

  render(){
    if (!this.state._loadingLogInDone) {
      return (
        <div>
          <p>Waiting, we're checking your logging profile</p>
        </div>
      )
    }
    else if (!this.state.isLoggedIn){
      alert('You are not allowed to be here')
      return <Redirect to="/" />;
    }
    else {
      return (
        <div>
          <HomeButton />
          <LogOutButton />
          <PresentationPhraseProfile />
          <Playlists />
        </div>
      )
    }
  }
}
