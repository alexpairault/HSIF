import React from 'react';
import {Titre} from './PresentationComponents/Titre.js'
import {Explanation} from './PresentationComponents/Explanation.js'
import {SignInButton} from './PresentationComponents/SignInButton.js'
import {BigThanksTo} from './PresentationComponents/BigThanksTo.js'
import {Footer} from './PresentationComponents/Footer.js'
import {Redirect} from 'react-router-dom'
import axios from 'axios'

export class Presentation extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isLoggedIn: undefined,
      logInLoadingDone: undefined
    }
  }

  async componentDidMount(){

    let promiseIsLoggedIn = await axios.get('/isLoggedIn')
    this.setState({ isLoggedIn: promiseIsLoggedIn.data.isLoggedIn })
    this.setState({ logInLoadingDone: true })
  }

  render(){
    if (!this.state.logInLoadingDone) {
      return (
        <p>Waiting...</p>
      )
    }
    else if (this.state.isLoggedIn) {
      return <Redirect to="/homepage" />;
    }
    else {
      return(
        <div className = "container-fluid">
          <Titre />
          <Explanation />
          <SignInButton />
          <BigThanksTo />
          <Footer />
        </div>
      )
    }
  }
}
