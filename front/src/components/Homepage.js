import React from 'react';
import {LogOutButton} from './ReusableComponents/LogOutButton.js'
import {ProfileButton} from './ReusableComponents/ProfileButton.js'
import {WelcomeMan} from './HomepageComponents/WelcomeMan.js'
import {Form} from './HomepageComponents/Form.js'
import {Redirect} from 'react-router-dom'
import axios from 'axios'

export class Homepage extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      displayName: '',
      isLoggedIn: undefined,
      logInLoadingDone: undefined,
      profileLoadingDone: undefined
    }
  }

  async componentDidMount(){
      /* reminder : an API call with axos inside componentWillMount will not return
     before the first render. This means it will render with empty data at least once ! e.g asynchronous ! It does not await
     and render.

     So we use componentDidMount. We know the render is already done so it is clear that I have
     to set up my state first.
     */
     let promiseIsLoggedIn = await axios.get('/isLoggedIn')
     this.setState({ isLoggedIn: promiseIsLoggedIn.data.isLoggedIn })
     this.setState({ logInLoadingDone: true })

     let promiseGetProfile = await axios.get('/getProfile')
     let fullName = promiseGetProfile.data.body.display_name
     let firstName = fullName.split(' ')[0]
     this.setState({ displayName: firstName })
     this.setState({ profileLoadingDone: true })

    }

  render(){
    if (!this.state.logInLoadingDone) {
      return (
        <div>
          <p>Loading...</p>
        </div>
      )
    }
    else if (!this.state.isLoggedIn){
      alert('You are not allowed to be here')
      return <Redirect to="/" />;
    }
    else if (!this.state.profileLoadingDone) {
      return (
        <p>Loading...</p>
      )
    }
    else {
      return(
        <div className = "container-fluid">
          <WelcomeMan displayName = {this.state.displayName}/>
          <Form />
          <ProfileButton />
          <LogOutButton />
        </div>
      )
    }
  }
}
