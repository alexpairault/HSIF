import React from 'react';

export class WelcomeMan extends React.Component {
  render(){
    return(
      <h1 class = "display-1 text-left">Hej {this.props.displayName}, what would you like for today?</h1>
    )
  }
}
