import React from 'react';
import Select from 'react-select'
import * as chicken from '../../images/chicken.json'
import Lottie from "react-lottie";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: chicken.default,
  rendererSettings: {
  preserveAspectRatio: "xMidYMid slice"
  }
}

const options = [
  { value: 'Paris', label: 'Paris' },
  { value: 'London', label: 'London' },
  { value: 'Stockholm', label: 'Stockholm' },
  { value: 'Berlin', label: 'Berlin'},
  { value: 'Delft', label: 'Delft'},
  { value: 'Toulouse', label: 'Toulouse'},
  { value: 'Oslo', label: 'Oslo'},
  { value: 'Copenhagen', label: 'Copenhagen'},
  { value: 'Amsterdam', label: 'Amsterdam'},
  { value: 'Niort', label: 'Niort'}
]

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '5px',
    padding: 20,
  })
}

export class Form extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      selectedOption: false,
      creatingNewPlaylist: false,
      resultCreationPlaylist: undefined
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
  }

  creatingNewPlaylist = (event) => {
    if (this.state.selectedOption.value === undefined){
      alert('You have to indicate a city')
      event.preventDefault()
    } else {
      this.setState({ creatingNewPlaylist: true })
    }
  }

  render(){
    const { selectedOption, creatingNewPlaylist } = this.state
    if (!creatingNewPlaylist){
      return(
        <div>
        <h1 class="display-4 text-left">I would like a Spotify playlist with artists that will perform in</h1>
          <div class="col-sm-3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              styles={customStyles}
              value={selectedOption}
              onChange={this.handleChange}
              options={options}
              placeholder="City..."
            />
          </div>
          <h1 class="display-4 text-left">so I <a onClick = {this.creatingNewPlaylist} href = {'http://localhost:8080/getUpcomingConcerts/' + this.state.selectedOption.value}>create</a> it</h1>
        </div>
      )
    } else {
      return(
        <div>
          <Lottie options={defaultOptions} height={220} width={220} />                      
          <p>This chicken is running as fast as possible to create you a playlist... </p>
        </div>
      )
    }
  }
}
