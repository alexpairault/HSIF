/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import axios from "axios";
import HSIF_London from '../../images/HSIF_London.png'
import HSIF_Stockholm from '../../images/HSIF_Stockholm.png'
import HSIF_Paris from '../../images/HSIF_Paris.png'
import HSIF_Amsterdam from '../../images/HSIF_Amsterdam.png'
import HSIF_Berlin from '../../images/HSIF_Berlin.png'
import Anonymous from '../../images/No_PP.png'
import * as loading from '../../images/loading.json'
import Lottie from "react-lottie";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: loading.default,
  rendererSettings: {
  preserveAspectRatio: "xMidYMid slice"
  }
}

export class Playlists extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      playlists: [],
      _loadingGettingPlaylistsDone: false
    }
  }

  async componentDidMount(){

    let promisePlaylists = await axios.get('/getHSIFPlaylists')
    let playlists = promisePlaylists.data.response
    this.setState({ playlists })
    this.setState({ _loadingGettingPlaylistsDone: true })

  }

  render(){
    if (!this.state._loadingGettingPlaylistsDone){
      return (
        <div>
          <Lottie options={defaultOptions} height={220} width={220} />                      
          <p>Few seconds, we are looking for your HSIF playlists</p>
        </div>
      )
    } else {
      return (
        <div>
          {this.state.playlists.map(playlist => (
            <div className="media mt-5" style = {{position: 'relative', overflow: 'hidden'}}>
              {(() => {
                switch (playlist.ppTown) {
                  case 'HSIF_Stockholm':   return <img className="ml-5" src={HSIF_Stockholm} alt="HSIF Stockholm image playlist" />
                  case 'HSIF_London': return <img className="ml-5" src={HSIF_London} alt="HSIF London image playlist" />
                  case 'HSIF_Paris': return <img className="ml-5" src={HSIF_Paris} alt="HSIF Paris image playlist" />
                  case 'HSIF_Amsterdam': return <img className="ml-5" src={HSIF_Amsterdam} alt="HSIF Amsterdam image playlist" />
                  case 'HSIF_Berlin': return <img className="ml-5" src={HSIF_Berlin} alt="HSIF Berlin image playlist" />
                  default: return "switch case image problem"
                }
              })()}
              <p className = "ml-5" style = {{position: 'absolute', color:'red'}}>Created the</p>
              <div className="media-body">
                <div className = "overflow-auto" style = {{height: 100+'%', position: 'absolute'}}>
                  <div className = "list-group">
                    {playlist.concerts.map(concert => (
                      <div className = "list-group-item list-group-item-action flex-column align-items-start">
                        <div className = "media">
                        {(() => {
                          if (concert.urlPPArtist) {
                            return <img className="ml-5 mt-5" style={{width: '160px', height: '160px'}}src={concert.urlPPArtist} alt="HSIF image playlist" />
                          } else {
                            return <img className="ml-5 mt-5" src={Anonymous} alt="Anonymous" />
                          }
                        })()}
                          <div className = "media-body">
                            <div className ="d-flex w-100 justify-content-between">
                              <p className="mt-1 ml-5"><strong>{concert.artistName}</strong> has {concert.numberOfSpotifyFollowers} followers on Spotify</p>
                                <small>Date of concert : {concert.date}
                                <br />
                                at : {concert.venueName}
                                </small>
                            </div>
                                <p className ="ml-5 mr-5">
                                <u>Here are some Spotify genre categories for this artist</u>
                                  <ul className = "list-group">
                                    <li className = "list-group-item">{concert.spotifyGenres[0]}</li>
                                    <li className = "list-group-item">{concert.spotifyGenres[1]}</li>
                                    <li className = "list-group-item">{concert.spotifyGenres[2]}</li>
                                  </ul>
                                </p>
                                <small>Go check more about this concert on <a href = {concert.songKickURI} target = "_blank">Songkick</a></small>
                          </div>
                        </div>
                      </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      )
    }
  }
}
