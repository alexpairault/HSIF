import React from 'react'
import axios from "axios";

export class PresentationPhraseProfile extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      display_name: '',
      email: '',
      pp: '',
      numberOfPlaylists: '',
      _loadingnumberOfPlaylistsDone: false
    }
  }

  async componentDidMount(){

    let promiseGetProfile = await axios.get('/getProfile')
    let fullName = promiseGetProfile.data.body.display_name
    let firstName = fullName.split(' ')[0]
    this.setState({
      display_name: firstName,
      email:  promiseGetProfile.data.body.email,
      pp:  promiseGetProfile.data.body.images[0].url
    })

    let promiseNumberOfPlaylists = await axios.get('/getNumberOfPlaylists')
    let number = promiseNumberOfPlaylists.data.response
    this.setState({ numberOfPlaylists: number })
    this.setState({ _loadingnumberOfPlaylistsDone: true })

  }

  render(){
    if (!this.state._loadingnumberOfPlaylistsDone){
      return (
        <p>Loading your personal data</p>
      )
    } else {
      return (
        <h1 className = "display-1 text-left ml-2">Hej {this.state.display_name}, you've got {this.state.numberOfPlaylists} HSIF playlists</h1>
      )
    }
  }
}
