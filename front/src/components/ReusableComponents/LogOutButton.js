import React from 'react'

export class LogOutButton extends React.Component {
  render(){
    return(
      <div>
        <h1 className="display-4 text-left ml-2">I would like to <a href = "http://localhost:8080/isLoggingOut">log out</a>
        </h1>
      </div>
    )
  }
}
