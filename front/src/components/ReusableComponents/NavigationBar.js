import React from 'react'

export class NavigationBar extends React.Component {
  render(){
    return(
      <nav class = 'navbar navbar-expand-lg navbar-light bg-light'>
        <a class = 'navbar-brand' href = '/'>Home</a>
        <a class = "navbar-brand" href = '/HSIF'>HSIF</a>
        <a class = "navbar-brand" href = '/HSIFplaylists'>Playlists</a>
        <a class = 'navbar-brand' href = '/profile'>Profile</a>
        <a class = 'navbar-brand' onClick = {this.logOut}>Log out</a>
      </nav>
    )
  }
}
