import React from 'react'

export class HomeButton extends React.Component {
  render(){
    return(
      <div>
        <h1 className="display-4 text-left ml-2">I would like to <a href = "http://localhost:3000/homepage">create a playlist</a>
        </h1>
      </div>
    )
  }
}
