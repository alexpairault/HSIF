import React from 'react';
import {Route, Switch} from 'react-router-dom'
import './App.css';
import {Homepage} from './components/Homepage.js'
import {Presentation} from './components/Presentation.js'
import {Profile} from './components/Profile.js'
import {Result} from './components/Result.js'

class App extends React.Component {
  render() {
    return (
      <div className = "App">
        <div className = "App-content">
          <Switch>
            <Route exact path = '/' component = {Presentation} />
            <Route exact path = '/profile' component = {Profile} />
            <Route exact path = '/homepage' component = {Homepage} />
            <Route exact path = '/result' component = {Result} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
