import axios from 'axios'

export default {
  signIn: () => {
    return axios.get('/signIn')
  },
  isLoggedIn: () => {
    return axios.get('/isLoggedIn')
  },
  logOut: () => {
    return axios.get('/isLoggingOut')
  }
}
