var mongoose = require('mongoose')
var playlistSchema = require('../Schema/schemaPlaylist.js')

const Playlist = mongoose.model('Playlist', playlistSchema)

module.exports = Playlist
