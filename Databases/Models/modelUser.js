var mongoose = require('mongoose')
var userSchema = require('../Schema/schemaUser.js')

const User = mongoose.model('User', userSchema)

module.exports = User
