const mongoose = require('mongoose')
var songConcert = require('./schemaConcert.js')

const playlistSchema = mongoose.Schema(
  {
    playlistId: {
      type: String,
      unique: true,
      required: true
    },
    creatorNameSpotifyEmail: {
      type: String,
      required: true
    },
    playlistName: String,
    concerts: [songConcert],
    town: String,
    ppTown: String
  },
  { timestamps: { createdAt: 'created_at' } }
)

module.exports = playlistSchema
