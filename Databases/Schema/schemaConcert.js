const mongoose = require('mongoose')

const schemaConcert = mongoose.Schema(
  {
    artistName: String,
    location: String,
    date: String,
    songKickURI: String,
    venueName: String,
    numberOfSpotifyFollowers: Number,
    spotifyGenres: Array,
    urlPPArtist: String
  }
)

module.exports = schemaConcert
