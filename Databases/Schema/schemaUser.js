const mongoose = require('mongoose')

const userSchema = mongoose.Schema(
  {
    email: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
      required: true
    },
    name: String
  },
  { timestamps: { createdAt: 'created_at' } }
)

module.exports = userSchema
