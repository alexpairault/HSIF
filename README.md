Use npm run dev to launch the app

# HSIF
How Should I Feel? is a project that I'm developing in the aim of improving my coding skills. It relates to music :) It is not finished yet but the purpose is to create Spotify playlists with artists that will perform in your town very soon (and your tastes but that feature is optional). 

For this project, I use : 
- Node.JS/Express.js
- React for the front (I'll do it at the end)
- MongoDB Atlas 
- Git/Github for versioning
- GitLab for a CI/CD deployment (still need to work on it -> still need to work on the tests part ! At first I wanted to follow a test driven development. I'll get back to it).
- Atom as text editor
- I'll use AWS to host my website.
- Spotify API 
- Songkick API

// Same, I should clean my git history
