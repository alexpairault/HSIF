var express = require('express')
var session = require('express-session')
var cors = require('cors')
// var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var mongoose = require('mongoose')
var controllers = require('./Controllers/index.js')
const configDatabase = require('./Config/configDatabase.js')

var app = express()

app.use(cors())
  .use(cookieParser())
  .use(session({ secret: 'javierPastoreExpress' }))
  .use('/', controllers.songkickController)
  .use('/', controllers.spotifyController)

app.listen(8080, function () {
  console.log('listening')
  mongoose.connect(`mongodb+srv://${configDatabase.userName}:${configDatabase.password}@cluster0-mj329.mongodb.net/test?retryWrites=true&w=majority`, { useNewUrlParser: true })
    .then(() => {
      console.log('I am connected to MongoDB')
    })
    .catch((err) => {
      console.log('error : ' + err)
    })
})

module.exports = app
