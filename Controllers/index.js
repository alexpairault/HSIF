// This file is used only to import from our controlers and export to our app.js
var spotifyController = require('./spotifyController.js')
var songkickController = require('./songkickController.js')

module.exports = {
  spotifyController: spotifyController,
  songkickController: songkickController
}
