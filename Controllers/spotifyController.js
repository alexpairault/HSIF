// require and import are doing the same job but import is ES6 and require is coming from CommonJS. For now, use of require is preferable.
var spotifyCredentials = require('../Config/configSpotify.js')
var express = require('express')
var spotifyController = express.Router()
const User = require('../Databases/Models/modelUser.js')
const Playlist = require('../Databases/Models/modelPlaylist.js')
// put the stateKey into the configure file
var stateKey = 'spotify_auth_state'
var randomstring = require('randomstring')
var SpotifyWebApi = require('spotify-web-api-node')
var scopes = ['user-read-private', 'user-read-email', 'playlist-modify-private', 'playlist-modify-public']
var date = require('date-and-time')

var spotifyApi = new SpotifyWebApi({
  clientId: spotifyCredentials.clientId,
  clientSecret: spotifyCredentials.clientSecret,
  redirectUri: spotifyCredentials.redirectUri
})

spotifyController.get('/signIn', (req, res) => {
  var state = randomstring.generate()
  res.cookie(stateKey, state)
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS')
  var authorizeURL = spotifyApi.createAuthorizeURL(scopes, state)
  res.redirect(authorizeURL)
})

spotifyController.get('/callback', async (req, res) => {
  const { code, state } = req.query || null
  const storedState = req.cookies ? req.cookies[stateKey] : null
  if (state === null || state !== storedState) {
    return res.status(500).json({
      error: 'states are not the same!'
    })
  } else {
    res.clearCookie(stateKey)
    try {
      var data = await spotifyApi.authorizationCodeGrant(code)
      const accessToken = data.body.access_token
      const refreshToken = data.body.refresh_token
      spotifyApi.setAccessToken(accessToken)
      spotifyApi.setRefreshToken(refreshToken)
      var profileData = await spotifyApi.getMe()
      var fullName = profileData.body.display_name
      var firstName = fullName.split(' ')[0]
      const userData = {
        name: firstName,
        email: profileData.body.email,
        id: profileData.body.id
      }

      try {
        // I insert a new user only if it is a new one
        const findUser = await User.findOne({ email: userData.email })
        if (!findUser) {
          const newUser = await new User(userData)
          try {
            newUser.save()
          } catch (error) {
            return res.status(500).json({
              error: 'error inserting user'
            })
          }
        }
        req.session.userData = userData
        res.redirect('http://localhost:3000/homepage')
      } catch (error) {
        return res.status(500).json({
          error: 'error find user'
        })
      }
    } catch (error) {
      return res.status(500).json({
        error: 'invalid token'
      })
    }
  }
})

spotifyController.get('/getProfile', async (req, res) => {
  try {
    var response = await spotifyApi.getMe()
    res.status(200).json(response)
  } catch (error) {
    res.status(500).json({
      error: 'problem with getting profile'
    })
  }
})

spotifyController.get('/getSpotifySongs', async (req, res) => {
  var uriSongsForPlaylist = []
  var i = 0
  var concertsInTown = req.session.concertsInTown
  while (i < concertsInTown.length) {
    var artistName = concertsInTown[i].artistName
    var response = await spotifyApi.searchTracks(`artist: ${artistName}`)
    if (response.body.tracks.total > 0) {
      const uriFirstTrackFromThisArtist = response.body.tracks.items[0].uri
      const idFromThisArtist = response.body.tracks.items[0].artists[0].id
      var promiseInformationAboutArtist = await spotifyApi.getArtist(idFromThisArtist)
      const numberOfSpotifyFollowers = promiseInformationAboutArtist.body.followers.total
      const spotifyGenres = promiseInformationAboutArtist.body.genres
      let urlPPArtist = ''

      if (promiseInformationAboutArtist.body.images.length > 0) {
        urlPPArtist = promiseInformationAboutArtist.body.images[2].url
      } else {
        urlPPArtist = 'anonymous'
      }
      concertsInTown[i].numberOfSpotifyFollowers = numberOfSpotifyFollowers
      concertsInTown[i].spotifyGenres = spotifyGenres
      concertsInTown[i].urlPPArtist = urlPPArtist
      // Not clean, I should update that
      uriSongsForPlaylist.push(uriFirstTrackFromThisArtist)
    }
    i++
  }
  req.session.uriSongsForPlaylist = uriSongsForPlaylist
  req.session.save()
  res.redirect('/createSpotifyPlaylist')
})

spotifyController.get('/createSpotifyPlaylist', async (req, res) => {
  // We first need to create a playlist
  var town = req.session.town

  try {
    var resultCreationPlaylist = await spotifyApi.createPlaylist(req.session.userData.id, `HSIF ${town} with love`, { public: false })
    var playlistId = resultCreationPlaylist.body.id
  } catch (error) {
    res.status(500).json({
      error: 'problem with the creation of a spotify playlist'
    })
  }
  var uriSongsForPlaylist = req.session.uriSongsForPlaylist
  // We now need to add the tracks to the playlist
  var playlistData = {
    playlistId: playlistId,
    creatorNameSpotifyEmail: req.session.userData.email,
    playlistName: `HSIF ${town} with love`,
    concerts: req.session.concertsInTown,
    town: town,
    ppTown: 'HSIF_' + town
  }

  try {
    const findPlaylist = await Playlist.findOne({ playlistId: playlistData.playlistId })
    if (!findPlaylist) {
      const newPlaylist = await new Playlist(playlistData)
      try {
        newPlaylist.save()
      } catch (error) {
        res.status(500).json({
          error: 'problem with saving the playlist'
        })
      }
    }
  } catch (error) {
    return res.status(500).json({
      error: 'there is alreayd a playlist with this id, why inserting again?'
    })
  }

  try {
    var resultPostingTracksIntoPlaylist = await spotifyApi.addTracksToPlaylist(playlistId, uriSongsForPlaylist)
    if (resultPostingTracksIntoPlaylist.statusCode === 201) {
      req.session.playlistCreationResult = true
      res.redirect('http://localhost:3000/result')
    } else {
      res.status(500).json({
        error: 'problem with the response given by spotify when adding tracks'
      })
    }
  } catch (error) {
    res.status(500).json({
      error: 'Problem with adding tracks to the playlist'
    })
  }
})

spotifyController.get('/getResultCreationOfPlaylist', async (req, res) => {
  try {
    res.status(200).json({
      response: req.session.playlistCreationResult
    })
  } catch (error) {
    res.status(500).json({
      error: 'cannot get the creation playlist result'
    })
  }
})

spotifyController.get('/getHSIFPlaylists', async (req, res) => {
  try {
    const findPlaylists = await Playlist.find({ creatorNameSpotifyEmail: req.session.userData.email })
    /*var i = 0
    while(i < findPlaylists.length){
      let actualDate = findPlaylists[i].created_at
      let belleDate = date.format(actualDate, 'MM/DD/YYYY')
      findPlaylists[i].belleDate = belleDate
    }
    */
    if (findPlaylists) {
      res.status(200).json({
        response: findPlaylists
      })
    } else {
      res.status(200).json({
        response: 'No playlists yet'
      })
    }
  } catch (error) {
    res.status(500).json({
      error: 'error when getting the spotify playlists'
    })
  }
})

spotifyController.get('/getNumberOfPlaylists', async (req, res) => {
  try {
    const numberOfPlaylists = await Playlist.countDocuments({ creatorNameSpotifyEmail: req.session.userData.email })
    res.status(200).json({
      response: numberOfPlaylists
    })
  } catch (error) {
    res.status(500).json({
      error: 'problem with getting the number of HSIF playlists'
    })
  }
})

spotifyController.get('/isLoggedIn', async (req, res) => {
  if (req.session.userData) {
    res.json({ isLoggedIn: true })
  } else {
    res.json({ isLoggedIn: false })
  }
})

spotifyController.get('/isLoggingOut', async (req, res) => {
  req.session.destroy()
  res.redirect('http://localhost:3000/')
})

spotifyController.get('/refreshToken', async (req, res) => {
  try {
    await spotifyApi.refreshAccessToken()
    console.log('token has been refreshed')
  } catch (error) {
    console.log('token not refreshed')
  }
})

module.exports = spotifyController
