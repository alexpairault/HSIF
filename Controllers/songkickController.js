var express = require('express')
var songkickController = express.Router()
var songkickCredentials = require('../Config/configSongkick.js')
const songkickAPIKey = songkickCredentials.key
var axios = require('axios')

const monthNames = [
  'january',
  'february',
  'march',
  'april',
  'may',
  'june',
  'july',
  'august',
  'september',
  'october',
  'november',
  'december'
]

songkickController.get('/getUpcomingConcerts/:town', async (req, res) => {
  var town = req.params.town
  try {
    // I need to get the songkick id for this specific town
    var responseIdTown = await axios.get(`https://api.songkick.com/api/3.0/search/locations.json?query=${town}&apikey=${songkickAPIKey}`)
    var idTown = responseIdTown.data.resultsPage.results.location[0].metroArea.id
  } catch (error) {
    console.log(error)
  }

  try {
    // I should put a min_date and max_date as noted in the songkick documentation
    var responseEventsInTown = await axios.get(`https://api.songkick.com/api/3.0/metro_areas/${idTown}/calendar.json?apikey=${songkickAPIKey}`)
    var regex = /.+?(?= at| with)/
    // Not clean?
    var i = 0
    var numberOfEventsInTown = responseEventsInTown.data.resultsPage.results.event.length
    var concertsInTown = []
    while (i < numberOfEventsInTown) {
      var artistName = await responseEventsInTown.data.resultsPage.results.event[i].displayName.match(regex)
      if (artistName) {
        artistName = artistName.join()
      }

      const uglyDate = responseEventsInTown.data.resultsPage.results.event[i].start.date.split('-')
      const beautfilMonth = monthNames[uglyDate[1] - 1]
      const beautfilDate = uglyDate[2] + ' of ' + beautfilMonth + ' ' + uglyDate[0]

      var concert = {
        artistName: artistName,
        location: town,
        date: beautfilDate,
        songKickURI: responseEventsInTown.data.resultsPage.results.event[i].uri,
        venueName: responseEventsInTown.data.resultsPage.results.event[i].venue.displayName
      }
      concertsInTown.push(concert)
      i++
    }
  } catch (error) {
    console.log(error)
  }
  req.session.concertsInTown = concertsInTown
  req.session.town = town
  req.session.save()
  res.redirect('http://localhost:8080/getSpotifySongs')
})

module.exports = songkickController
